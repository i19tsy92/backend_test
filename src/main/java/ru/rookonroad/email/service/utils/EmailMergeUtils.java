package ru.rookonroad.email.service.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class EmailMergeUtils {

    public static Map<String, Set<String>> uniqueUsersList(Map<String, List<String>> entry) {

        if (entry == null) {
            throw new IllegalArgumentException();
        }

        Map<String, Set<String>> result = new HashMap<>();

        if (entry.isEmpty()) {
            return result;
        }

        for (String key: entry.keySet()) {
            AtomicBoolean flag = new AtomicBoolean(false);
            result.forEach((user, value) -> {
                for (String email: entry.get(key)) {
                    if (value.contains(email)) {
                        flag.getAndSet(true);
                        value.addAll(entry.get(key));
                        break;
                    }
                }
            });
            if (!flag.get()) {
                result.put(key, new HashSet<>(entry.get(key)));
            }
        }
        return result;
    }
}
