package ru.rookonroad.email.service.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class IOUtils {

    public static List<String> parseEmailList(String emailStr) {
        String[] array = emailStr.split(",");
        List<String> result = new ArrayList<>();
        for (String s : array) {
            result.add(s.trim());
        }
        return result;
    }

    public static String convertOutputResult(Map<String, Set<String>> userEmails) {
        StringBuilder result = new StringBuilder();
        userEmails.forEach((k,v) -> {
            result.append(k);
            result.append(" -> ");
            v.forEach(value -> {
                result.append(value).append(", ");
            });
            result.deleteCharAt(result.length()-2);
            result.append("\n");
        });
        return result.toString();
    }
}
