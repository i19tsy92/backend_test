package ru.rookonroad.email.service;

import ru.rookonroad.email.service.utils.EmailMergeUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import static ru.rookonroad.email.service.utils.IOUtils.convertOutputResult;
import static ru.rookonroad.email.service.utils.IOUtils.parseEmailList;

public class TestApplication {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, List<String>> userEmailList = new HashMap<>();
        System.out.println("Input: users with email lists");
        while (scanner.hasNextLine()) {
            String buffer = scanner.nextLine();
            String[] expression = buffer.split("->");
            if (!buffer.equals("")) {
                userEmailList.put(expression[0].trim(), parseEmailList(expression[1].trim()));
            } else {
                break;
            }
        }
        scanner.close();
        Map<String, Set<String>> result = EmailMergeUtils.uniqueUsersList(userEmailList);
        System.out.println(convertOutputResult(result));
    }


}
