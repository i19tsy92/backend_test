package test;

import org.junit.Test;
import ru.rookonroad.email.service.utils.EmailMergeUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class EmailMergeUtilsUnitTests {

    @Test
    public void testEmptyCase() {
        Map<String, List<String>> userEmail = new HashMap<>();
        assert EmailMergeUtils.uniqueUsersList(userEmail).isEmpty();
        assert checkResult(EmailMergeUtils.uniqueUsersList(userEmail));
    }

    @Test
    public void testBasicCase() {
        Map<String, List<String>> userEmail = new HashMap<String, List<String>>() {{
            put("user1", new ArrayList<String>() {{
                add("xxx@ya.ru");
                add("foo@gmail.com");
                add("lol@mail.ru");
            }});
            put("user2", new ArrayList<String>() {{
                add("foo@gmail.com");
                add("ups@pisem.net");
            }});
            put("user3", new ArrayList<String>() {{
                add("xyz@pisem.net");
                add("vasya@pupkin.com");
            }});
            put("user4", new ArrayList<String>() {{
                add("ups@pisem.net");
                add("aaa@bbb.ru");
            }});
            put("user5", new ArrayList<String>() {{
                add("xyz@pisem.net");
            }});
        }};

        Map<String, Set<String>> result = EmailMergeUtils.uniqueUsersList(userEmail);
        System.out.println(result);
        assert checkResult(result);
    }

    @Test
    public void testAllNotEquals() {
        Map<String, List<String>> userEmail = new HashMap<String, List<String>>() {{
            put("user1", new ArrayList<String>() {{
                add("xxx@ya.ru");
                add("lol@mail.ru");
            }});
            put("user2", new ArrayList<String>() {{
                add("foo@gmail.com");
                add("ups@pisem.net");
            }});
            put("user3", new ArrayList<String>() {{
                add("xyz@pisem.net");
                add("vasya@pupkin.com");
            }});
            put("user4", new ArrayList<String>() {{
                add("ups2@pisem.net");
                add("aaa@bbb.ru");
            }});
            put("user5", new ArrayList<String>() {{
                add("xyzg@pisem.net");
            }});
        }};
        System.out.println(EmailMergeUtils.uniqueUsersList(userEmail));
        assert EmailMergeUtils.uniqueUsersList(userEmail).size() == userEmail.size();
        assert checkResult(EmailMergeUtils.uniqueUsersList(userEmail));
    }

    @Test
    public void testAlternativeCases() {
        Map<String, List<String>> userEmail = new HashMap<String, List<String>>() {{
            put("user1", new ArrayList<String>() {{
                add("xxx@ya.ru");
                add("lol@mail.ru");
            }});
            put("user2", new ArrayList<String>() {{
                add("foo@gmail.com");
                add("ups@pisem.net");
                add("xyz@pisem.net");
            }});
            put("user3", new ArrayList<String>() {{
                add("xyz@pisem.net");
                add("vasya@pupkin.com");
            }});
            put("user4", new ArrayList<String>() {{
                add("ups2@pisem.net");
                add("aaa@bbb.ru");
            }});
            put("user5", new ArrayList<String>() {{
                add("aaa@bbb.ru");
                add("xyzg@pisem.net");
            }});
        }};
        System.out.println(EmailMergeUtils.uniqueUsersList(userEmail));
        assert EmailMergeUtils.uniqueUsersList(userEmail).size() == 3;
        assert checkResult(EmailMergeUtils.uniqueUsersList(userEmail));
    }

    @Test
    public void testAllEqualsCase() {
        Map<String, List<String>> userEmail = new HashMap<String, List<String>>() {{
            put("user1", new ArrayList<String>() {{
                add("ups@pisem.net");
                add("aaa@bbb.ru");
            }});
            put("user2", new ArrayList<String>() {{
                add("foo@gmail.com");
                add("ups@pisem.net");
            }});
            put("user3", new ArrayList<String>() {{
                add("ups@pisem.net");
                add("aaa@bbb.ru");
            }});
            put("user4", new ArrayList<String>() {{
                add("ups@pisem.net");
                add("aaa@bbb.ru");
            }});
            put("user5", new ArrayList<String>() {{
                add("ups@pisem.net");
                add("aaa@bbb.ru");
            }});
        }};
        assert checkResult(EmailMergeUtils.uniqueUsersList(userEmail));
    }

    @Test
    public void testNullCase() {
        assert !testException(null);
    }

    public boolean testException(Map<String, List<String>> entry) {
        try {
            EmailMergeUtils.uniqueUsersList(entry);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    public boolean checkResult(Map<String, Set<String>> result) {
        AtomicBoolean res = new AtomicBoolean(true);
        for (String key : result.keySet()) {
            for (String key2: result.keySet()) {
                if (!key.equals(key2)){
                    result.get(key2).forEach(second -> {
                        if (result.get(key).contains(second)) {
                            res.getAndSet(false);
                        }
                    });
                }
            }
        }
        return res.get();
    }
}
