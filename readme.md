# Консольное Java приложение для получения списка уникальных пользователей
# Инструкция по сборке и запуску: 
* Перейти в директорию приложения и выполнить компанду mvn assembly:assembly
* После успешной сборки перейти в папку target и выполнить java -jar backend_test-1.0-SNAPSHOT-jar-with-dependencies.jar


# Пример формата входных данных
* user1 -> xxx@ya.ru, foo@gmail.com, lol@mail.ru
* user2 -> foo@gmail.com, ups@pisem.net
* user3 -> xyz@pisem.net, vasya@pupkin.com
* user4 -> ups@pisem.net, aaa@bbb.ru
* user5 -> xyz@pisem.net
# Пример формата выходных данных
* user1 -> xxx@ya.ru, foo@gmail.com, lol@mail.ru, ups@pisem.net, aaa@bbb.ru
* user3 -> xyz@pisem.net, vasya@pupkin.com
